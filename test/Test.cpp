#include "cute/cute.h"
#include "cute/ide_listener.h"
#include "cute/xml_listener.h"
#include "cute/cute_runner.h"

#include "calc.h"
#include "sevensegment.h"

using namespace std;

void testOperators() {
    ASSERT_EQUAL(1 + 1, calc(1, 1, '+'));
    ASSERT_EQUAL(1 - 1, calc(1, 1, '-'));
    ASSERT_EQUAL(2 * 21, calc(2, 21, '*'));
    ASSERT_EQUAL(42 / 7, calc(42, 7, '/'));
    ASSERT_EQUAL(12 % 5, calc(12, 5, '%'));
}

void testInputStreamCalculation() {
    istringstream input{ "12 * 23" };
    const int result = calc(input);
    ASSERT_EQUAL(12 * 23, result);
}

void testUnknownOperation() {
    ASSERT_THROWS(calc(1, 2, '?'), invalid_argument);
}

void testDivideByZero() {
    ASSERT_THROWS(calc(3, 0, '/'), invalid_argument);
    ASSERT_THROWS(calc(3, 0, '%'), invalid_argument);
}

void testIllegalInputStream() {
    vector<istringstream> inputs{};

    inputs.emplace_back("7");
    inputs.emplace_back("3+");
    inputs.emplace_back("+3");
    inputs.emplace_back("nonsense");

    for_each(inputs.begin(), inputs.end(), [](istringstream &in) {
        ASSERT_THROWS(calc(in), invalid_argument);
    });
}

void testPrintLargeDigitZero() {
    ostringstream output{};
    printLargeDigit(0, output);
    ASSERT_EQUAL(" - \n"
                 "| |\n"
                 "   \n"
                 "| |\n"
                 " - \n", output.str());
}

bool runAllTests(int argc, char const *argv[]) {
    cute::suite s{};

    s.push_back(CUTE(testOperators));
    s.push_back(CUTE(testInputStreamCalculation));

    s.push_back(CUTE(testUnknownOperation));
    s.push_back(CUTE(testDivideByZero));
    s.push_back(CUTE(testIllegalInputStream));

    s.push_back(CUTE(testPrintLargeDigitZero));

    cute::xml_file_opener xmlfile(argc, argv);
    cute::xml_listener<cute::ide_listener<>> lis(xmlfile.out);
    auto runner{ cute::makeRunner(lis, argc, argv) };
    bool success = runner(s, "AllTests");
    return success;
}

int main(int argc, char const *argv[]) {
    return runAllTests(argc, argv) ? EXIT_SUCCESS : EXIT_FAILURE;
}