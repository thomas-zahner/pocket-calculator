#pragma once

#include <iosfwd>
#include <array>
#include <vector>
#include <functional>

#define DIGIT_HEIGHT 5

using large_digit = std::array<std::string, DIGIT_HEIGHT>;

void printLargeNumber(int number, std::ostream &out);

void printLargeDigit(int digit, std::ostream &out);

void printLargeError(std::ostream &out);
