#include "calc.h"

#include <stdexcept>
#include <istream>

int calc(std::istream &in) {
    int left{}, right{};
    char operation{};

    in >> left;
    in >> operation;
    in >> right;

    if (in.fail()) {
        throw std::invalid_argument{ "Unable to interpret input" };
    }

    return calc(left, right, operation);
}

int calc(int left, int right, char operation) {
    if (right == 0 && (operation == '/' || operation == '%')) {
        throw std::invalid_argument{ "Cannot divide by zero" };
    }

    switch (operation) {
        case '+':
            return left + right;
        case '-':
            return left - right;
        case '*':
            return left * right;
        case '/':
            return left / right;
        case '%':
            return left % right;
        default:
            throw std::invalid_argument{ "Unsupported operation" };
    }
}
