#include "sevensegment.h"

#include <ostream>
#include <stdexcept>
#include <algorithm>
#include <vector>
#include <array>

using namespace std;

void print(vector<large_digit> &digits, ostream &out);

large_digit digitToLargeDigit(int digit);

void numberToLargeDigits(int number, std::vector<large_digit> &digits);

void printLargeNumber(int number, ostream &out) {
    vector<large_digit> digits{};

    if (number < 0) {
        large_digit minus = { "   ", "   ", " - ", "   ", "   " };
        digits.push_back(minus);
        number = -number;
    }

    numberToLargeDigits(number, digits);
    print(digits, out);
}

void printLargeDigit(int digit, ostream &out) {
    vector<large_digit> digits = { digitToLargeDigit(digit) };
    print(digits, out);
}

void printLargeError(ostream &out) {
    large_digit E = { " - ", "|  ", " - ", "|  ", " - " };
    large_digit r = { "   ", "   ", " - ", "|  ", "   " };
    large_digit o = { "   ", "   ", " - ", "| |", " - " };

    vector<large_digit> errorMessage = { E, r, r, o, r };
    print(errorMessage, out);
}

void print(vector<large_digit> &digits, ostream &out) {
    for (int i = 0; i < DIGIT_HEIGHT; ++i) {
        for_each(digits.cbegin(), digits.cend(), [ &out, i ](auto &digit) {
            out << digit.at(i);
        });

        out << '\n';
    }
}

large_digit digitToLargeDigit(int digit) {
    if (digit < 0 || digit > 9) {
        throw invalid_argument{ "Only single digits from zero to nine are allowed" };
    }

    array<large_digit, 10> digits{};

    digits.at(0) = { " - ", "| |", "   ", "| |", " - " };
    digits.at(1) = { "   ", "  |", "   ", "  |", "   " };
    digits.at(2) = { " - ", "  |", " - ", "|  ", " - " };
    digits.at(3) = { " - ", "  |", " - ", "  |", " - " };
    digits.at(4) = { "   ", "| |", " - ", "  |", "   " };
    digits.at(5) = { " - ", "|  ", " - ", "  |", " - " };
    digits.at(6) = { " - ", "|  ", " - ", "| |", " - " };
    digits.at(7) = { " - ", "  |", "   ", "  |", "   " };
    digits.at(8) = { " - ", "| |", " - ", "| |", " - " };
    digits.at(9) = { " - ", "| |", " - ", "  |", " - " };

    return digits.at(digit);
}

void numberToLargeDigits(int number, vector<large_digit> &digits) {
    if (number > 9) {
        numberToLargeDigits(number / 10, digits);
    }

    digits.push_back(digitToLargeDigit(number % 10));
}
